rust-gimli (0.31.1-2) unstable; urgency=medium

  * Team upload.
  * Package gimli 0.31.1 from crates.io using debcargo 2.7.5
  * Upload to unstable (Closes: #1073006)

 -- Peter Michael Green <plugwash@debian.org>  Wed, 25 Dec 2024 17:25:15 +0000

rust-gimli (0.31.1-1) experimental; urgency=medium

  * Team upload.
  * Package gimli 0.31.1 from crates.io using debcargo 2.7.5
  * Update fix-tests-no-default-features.patch for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 17 Dec 2024 19:25:02 +0000

rust-gimli (0.28.1-2) unstable; urgency=medium

  * Team upload.
  * Package gimli 0.28.1 from crates.io using debcargo 2.6.1

 -- Peter Michael Green <plugwash@debian.org>  Fri, 05 Jan 2024 23:06:25 +0000

rust-gimli (0.28.1-1) experimental; urgency=medium

  * Team upload.
  * Package gimli 0.28.1 from crates.io using debcargo 2.6.1
  * Drop remove-wasm-from-object-dev-depends.patch, upstream no longer has
    a dev-dependency on object.
  * Drop patch for memmap2 dev-dependency, upstream no longer depends on
    memmap2
  * Relax indexmap dependency to ">= 1.9.3".
  * Update remaining patches for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Fri, 24 Nov 2023 16:03:05 +0000

rust-gimli (0.27.3-1) unstable; urgency=medium

  * Package gimli 0.27.3 from crates.io using debcargo 2.6.0
  * Relax dev-dependency on memmap2.

  [ Fabian Grünbichler ]
  * Team upload.
  * Package gimli 0.27.3 from crates.io using debcargo 2.6.0

 -- Peter Michael Green <plugwash@debian.org>  Mon, 28 Aug 2023 12:01:00 +0000

rust-gimli (0.27.0-1) unstable; urgency=medium

  * Team upload.
  * Package gimli 0.27.0 from crates.io using debcargo 2.6.0
  * Update remove-wasm-from-object-dev-depends.patch for new upstream.
  * Update remove-test-assembler.patch for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 02 Jan 2023 11:13:07 +0000

rust-gimli (0.26.2-1) unstable; urgency=medium

  * Team upload.
  * Package gimli 0.26.2 from crates.io using debcargo 2.5.0
  * Remove "wasm" feature from "object" dev-dependency, it doesn't seem to be
    needed at least on linux.
  * Fix tests with --no-default-features.
  * Remove dev-dependency on test-assembler and disable tests that depend on
    it so the rest of the tests can run.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 21 Aug 2022 01:56:57 +0000

rust-gimli (0.22.0-1) experimental; urgency=medium

  * Team upload.
  * Package gimli 0.22.0 from crates.io using debcargo 2.4.4
  * Drop relax-dep.patch no longer needed.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 02 Nov 2021 04:44:40 +0000

rust-gimli (0.19.0-2) unstable; urgency=medium

  * Package gimli 0.19.0 from crates.io using debcargo 2.4.0

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Wed, 01 Jan 2020 14:34:27 +0100

rust-gimli (0.19.0-1) unstable; urgency=medium

  * Package gimli 0.19.0 from crates.io using debcargo 2.4.0

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Sat, 30 Nov 2019 13:34:44 +0100

rust-gimli (0.16.1-1) unstable; urgency=medium

  * Package gimli 0.16.1 from crates.io using debcargo 2.2.9

 -- Ximin Luo <infinity0@debian.org>  Tue, 18 Dec 2018 16:56:51 -0800
