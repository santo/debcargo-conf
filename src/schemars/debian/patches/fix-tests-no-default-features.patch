diff --git a/Cargo.toml b/Cargo.toml
index 87b34f8..5efbfc0 100644
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -33,15 +33,15 @@ all-features = true
 
 [[test]]
 name = "chrono"
-required-features = ["chrono"]
+required-features = ["chrono","derive"]
 
 [[test]]
 name = "indexmap"
-required-features = ["indexmap"]
+required-features = ["indexmap","derive"]
 
 [[test]]
 name = "indexmap2"
-required-features = ["indexmap2"]
+required-features = ["indexmap2","derive"]
 
 [[test]]
 name = "either"
@@ -73,15 +73,15 @@ required-features = ["impl_json_schema"]
 
 [[test]]
 name = "ui"
-required-features = ["ui_test"]
+required-features = ["ui_test","derive"]
 
 [[test]]
 name = "url"
-required-features = ["url"]
+required-features = ["url","derive"]
 
 [[test]]
 name = "enumset"
-required-features = ["enumset"]
+required-features = ["enumset","derive"]
 
 [[test]]
 name = "smol_str"
@@ -89,7 +89,7 @@ required-features = ["smol_str"]
 
 [[test]]
 name = "semver"
-required-features = ["semver"]
+required-features = ["semver","derive"]
 
 [[test]]
 name = "decimal"
@@ -98,6 +98,130 @@ required-features = [
     "bigdecimal04",
 ]
 
+[[test]]
+name = "bound"
+required-features = ["derive"]
+
+[[test]]
+name = "crate_alias"
+required-features = ["derive"]
+
+[[test]]
+name = "default"
+required-features = ["derive"]
+
+[[test]]
+name = "deprecated"
+required-features = ["derive"]
+
+[[test]]
+name = "dereference"
+required-features = ["derive"]
+
+[[test]]
+name = "docs"
+required-features = ["derive"]
+
+[[test]]
+name = "enum"
+required-features = ["derive"]
+
+[[test]]
+name = "enum_deny_unknown_fields"
+required-features = ["derive"]
+
+[[test]]
+name = "enum_repr"
+required-features = ["derive"]
+
+[[test]]
+name = "examples"
+required-features = ["derive"]
+
+[[test]]
+name = "flatten"
+required-features = ["derive"]
+
+[[test]]
+name = "ffi"
+required-features = ["derive"]
+
+[[test]]
+name = "inline_subschemas"
+required-features = ["derive"]
+
+[[test]]
+name = "macro"
+required-features = ["derive"]
+
+[[test]]
+name = "nonzero_ints"
+required-features = ["derive"]
+
+[[test]]
+name = "property_name"
+required-features = ["derive"]
+
+[[test]]
+name = "range"
+required-features = ["derive"]
+
+[[test]]
+name = "remote_derive"
+required-features = ["derive"]
+
+[[test]]
+name = "remote_derive_generic"
+required-features = ["derive"]
+
+[[test]]
+name = "result"
+required-features = ["derive"]
+
+[[test]]
+name = "same_name"
+required-features = ["derive"]
+
+[[test]]
+name = "schema_name"
+required-features = ["derive"]
+
+[[test]]
+name = "schema_with_enum"
+required-features = ["derive"]
+
+[[test]]
+name = "schema_with_struct"
+required-features = ["derive"]
+
+[[test]]
+name = "skip"
+required-features = ["derive"]
+
+[[test]]
+name = "struct"
+required-features = ["derive"]
+
+[[test]]
+name = "struct_additional_properties"
+required-features = ["derive"]
+
+[[test]]
+name = "time"
+required-features = ["derive"]
+
+[[test]]
+name = "transparent"
+required-features = ["derive"]
+
+[[test]]
+name = "validate"
+required-features = ["derive"]
+
+[[test]]
+name = "validate_inner"
+required-features = ["derive"]
+
 [dependencies.arrayvec07]
 version = "0.7"
 optional = true
diff --git a/README.md b/README.md
index 2ff1837..8c39dba 100644
--- a/README.md
+++ b/README.md
@@ -12,6 +12,8 @@ Generate JSON Schema documents from Rust code
 If you don't really care about the specifics, the easiest way to generate a JSON schema for your types is to `#[derive(JsonSchema)]` and use the `schema_for!` macro. All fields of the type must also implement `JsonSchema` - Schemars implements this for many standard library types.
 
 ```rust
+#[cfg(feature = "derive")]
+{
 use schemars::{schema_for, JsonSchema};
 
 #[derive(JsonSchema)]
@@ -29,6 +31,7 @@ pub enum MyEnum {
 
 let schema = schema_for!(MyStruct);
 println!("{}", serde_json::to_string_pretty(&schema).unwrap());
+}
 ```
 
 <details>
@@ -105,6 +108,8 @@ println!("{}", serde_json::to_string_pretty(&schema).unwrap());
 One of the main aims of this library is compatibility with [Serde](https://github.com/serde-rs/serde). Any generated schema _should_ match how [serde_json](https://github.com/serde-rs/json) would serialize/deserialize to/from JSON. To support this, Schemars will check for any `#[serde(...)]` attributes on types that derive `JsonSchema`, and adjust the generated schema accordingly.
 
 ```rust
+#[cfg(feature = "derive")]
+{
 use schemars::{schema_for, JsonSchema};
 use serde::{Deserialize, Serialize};
 
@@ -127,6 +132,7 @@ pub enum MyEnum {
 
 let schema = schema_for!(MyStruct);
 println!("{}", serde_json::to_string_pretty(&schema).unwrap());
+}
 ```
 
 <details>
diff --git a/examples/custom_serialization.rs b/examples/custom_serialization.rs
index 53c78fa..d892f94 100644
--- a/examples/custom_serialization.rs
+++ b/examples/custom_serialization.rs
@@ -1,60 +1,64 @@
-use schemars::schema::{Schema, SchemaObject};
-use schemars::{gen::SchemaGenerator, schema_for, JsonSchema};
-use serde::{Deserialize, Serialize};
-
-// `int_as_string` and `bool_as_string` use the schema for `String`.
-#[derive(Default, Deserialize, Serialize, JsonSchema)]
-pub struct MyStruct {
-    #[serde(default = "eight", with = "as_string")]
-    #[schemars(with = "String")]
-    pub int_as_string: i32,
-
-    #[serde(default = "eight")]
-    pub int_normal: i32,
-
-    #[serde(default, with = "as_string")]
-    #[schemars(schema_with = "make_custom_schema")]
-    pub bool_as_string: bool,
-
-    #[serde(default)]
-    pub bool_normal: bool,
-}
+#[cfg(feature = "derive")]
+fn main() {
+    use schemars::schema::{Schema, SchemaObject};
+    use schemars::{gen::SchemaGenerator, schema_for, JsonSchema};
+    use serde::{Deserialize, Serialize};
 
-fn make_custom_schema(gen: &mut SchemaGenerator) -> Schema {
-    let mut schema: SchemaObject = <String>::json_schema(gen).into();
-    schema.format = Some("boolean".to_owned());
-    schema.into()
-}
+    // `int_as_string` and `bool_as_string` use the schema for `String`.
+    #[derive(Default, Deserialize, Serialize, JsonSchema)]
+    pub struct MyStruct {
+	#[serde(default = "eight", with = "as_string")]
+	#[schemars(with = "String")]
+	pub int_as_string: i32,
 
-fn eight() -> i32 {
-    8
-}
+	#[serde(default = "eight")]
+	pub int_normal: i32,
 
-// This module serializes values as strings
-mod as_string {
-    use serde::{de::Error, Deserialize, Deserializer, Serializer};
+	#[serde(default, with = "as_string")]
+	#[schemars(schema_with = "make_custom_schema")]
+	pub bool_as_string: bool,
 
-    pub fn serialize<T, S>(value: &T, serializer: S) -> Result<S::Ok, S::Error>
-    where
-        T: std::fmt::Display,
-        S: Serializer,
-    {
-        serializer.collect_str(value)
+	#[serde(default)]
+	pub bool_normal: bool,
     }
 
-    pub fn deserialize<'de, T, D>(deserializer: D) -> Result<T, D::Error>
-    where
-        T: std::str::FromStr,
-        D: Deserializer<'de>,
-    {
-        let string = String::deserialize(deserializer)?;
-        string
-            .parse()
-            .map_err(|_| D::Error::custom("Input was not valid"))
+    fn make_custom_schema(gen: &mut SchemaGenerator) -> Schema {
+	let mut schema: SchemaObject = <String>::json_schema(gen).into();
+	schema.format = Some("boolean".to_owned());
+	schema.into()
+    }
+
+    fn eight() -> i32 {
+	8
+    }
+
+    // This module serializes values as strings
+    mod as_string {
+	use serde::{de::Error, Deserialize, Deserializer, Serializer};
+
+	pub fn serialize<T, S>(value: &T, serializer: S) -> Result<S::Ok, S::Error>
+	where
+            T: std::fmt::Display,
+            S: Serializer,
+	{
+            serializer.collect_str(value)
+	}
+
+	pub fn deserialize<'de, T, D>(deserializer: D) -> Result<T, D::Error>
+	where
+            T: std::str::FromStr,
+            D: Deserializer<'de>,
+	{
+            let string = String::deserialize(deserializer)?;
+            string
+		.parse()
+		.map_err(|_| D::Error::custom("Input was not valid"))
+	}
     }
-}
 
-fn main() {
     let schema = schema_for!(MyStruct);
     println!("{}", serde_json::to_string_pretty(&schema).unwrap());
 }
+
+#[cfg(not(feature = "derive"))]
+fn main() {}
diff --git a/examples/custom_settings.rs b/examples/custom_settings.rs
index 335daf3..c765de4 100644
--- a/examples/custom_settings.rs
+++ b/examples/custom_settings.rs
@@ -1,19 +1,20 @@
-use schemars::{gen::SchemaSettings, JsonSchema};
+#[cfg(feature = "derive")]
+fn main() {
+    use schemars::{gen::SchemaSettings, JsonSchema};
 
-#[derive(JsonSchema)]
-pub struct MyStruct {
-    pub my_int: i32,
-    pub my_bool: bool,
-    pub my_nullable_enum: Option<MyEnum>,
-}
+    #[derive(JsonSchema)]
+    pub struct MyStruct {
+	pub my_int: i32,
+	pub my_bool: bool,
+	pub my_nullable_enum: Option<MyEnum>,
+    }
 
-#[derive(JsonSchema)]
-pub enum MyEnum {
-    StringNewType(String),
-    StructVariant { floats: Vec<f32> },
-}
+    #[derive(JsonSchema)]
+    pub enum MyEnum {
+	StringNewType(String),
+	StructVariant { floats: Vec<f32> },
+    }
 
-fn main() {
     let settings = SchemaSettings::draft07().with(|s| {
         s.option_nullable = true;
         s.option_add_null_type = false;
@@ -22,3 +23,6 @@ fn main() {
     let schema = gen.into_root_schema_for::<MyStruct>();
     println!("{}", serde_json::to_string_pretty(&schema).unwrap());
 }
+
+#[cfg(not(feature = "derive"))]
+fn main() {}
diff --git a/examples/doc_comments.rs b/examples/doc_comments.rs
index c3faa4f..39e02d9 100644
--- a/examples/doc_comments.rs
+++ b/examples/doc_comments.rs
@@ -1,33 +1,37 @@
-use schemars::{schema_for, JsonSchema};
+#[cfg(feature = "derive")]
+fn main() {
+    use schemars::{schema_for, JsonSchema};
 
-/// # My Amazing Struct
-/// This struct shows off generating a schema with
-/// a custom title and description.
-#[derive(JsonSchema)]
-pub struct MyStruct {
-    /// # My Amazing Integer
-    pub my_int: i32,
-    /// This bool has a description, but no title.
-    pub my_bool: bool,
-    /// # A Nullable Enum
-    /// This enum might be set, or it might not.
-    pub my_nullable_enum: Option<MyEnum>,
-}
+    /// # My Amazing Struct
+    /// This struct shows off generating a schema with
+    /// a custom title and description.
+    #[derive(JsonSchema)]
+    pub struct MyStruct {
+	/// # My Amazing Integer
+	pub my_int: i32,
+	/// This bool has a description, but no title.
+	pub my_bool: bool,
+	/// # A Nullable Enum
+	/// This enum might be set, or it might not.
+	pub my_nullable_enum: Option<MyEnum>,
+    }
 
-/// # My Amazing Enum
-#[derive(JsonSchema)]
-pub enum MyEnum {
-    /// A wrapper around a `String`
-    StringNewType(String),
-    /// A struct-like enum variant which contains
-    /// some floats
-    StructVariant {
-        /// The floats themselves
-        floats: Vec<f32>,
-    },
-}
+    /// # My Amazing Enum
+    #[derive(JsonSchema)]
+    pub enum MyEnum {
+	/// A wrapper around a `String`
+	StringNewType(String),
+	/// A struct-like enum variant which contains
+	/// some floats
+	StructVariant {
+            /// The floats themselves
+            floats: Vec<f32>,
+	},
+    }
 
-fn main() {
     let schema = schema_for!(MyStruct);
     println!("{}", serde_json::to_string_pretty(&schema).unwrap());
 }
+
+#[cfg(not(feature = "derive"))]
+fn main() {}
diff --git a/examples/enum_repr.rs b/examples/enum_repr.rs
index 3b0df97..814738c 100644
--- a/examples/enum_repr.rs
+++ b/examples/enum_repr.rs
@@ -1,15 +1,19 @@
-use schemars::{schema_for, JsonSchema_repr};
+#[cfg(feature = "derive")]
+fn main() {
+    use schemars::{schema_for, JsonSchema_repr};
 
-#[derive(JsonSchema_repr)]
-#[repr(u8)]
-enum SmallPrime {
-    Two = 2,
-    Three = 3,
-    Five = 5,
-    Seven = 7,
-}
+    #[derive(JsonSchema_repr)]
+    #[repr(u8)]
+    enum SmallPrime {
+	Two = 2,
+	Three = 3,
+	Five = 5,
+	Seven = 7,
+    }
 
-fn main() {
     let schema = schema_for!(SmallPrime);
     println!("{}", serde_json::to_string_pretty(&schema).unwrap());
 }
+
+#[cfg(not(feature = "derive"))]
+fn main() {}
diff --git a/examples/main.rs b/examples/main.rs
index 58db8b9..ca37c4a 100644
--- a/examples/main.rs
+++ b/examples/main.rs
@@ -1,19 +1,23 @@
-use schemars::{schema_for, JsonSchema};
+#[cfg(feature = "derive")]
+fn main() {
+    use schemars::{schema_for, JsonSchema};
 
-#[derive(JsonSchema)]
-pub struct MyStruct {
-    pub my_int: i32,
-    pub my_bool: bool,
-    pub my_nullable_enum: Option<MyEnum>,
-}
+    #[derive(JsonSchema)]
+    pub struct MyStruct {
+	pub my_int: i32,
+	pub my_bool: bool,
+	pub my_nullable_enum: Option<MyEnum>,
+    }
 
-#[derive(JsonSchema)]
-pub enum MyEnum {
-    StringNewType(String),
-    StructVariant { floats: Vec<f32> },
-}
+    #[derive(JsonSchema)]
+    pub enum MyEnum {
+	StringNewType(String),
+	StructVariant { floats: Vec<f32> },
+    }
 
-fn main() {
     let schema = schema_for!(MyStruct);
     println!("{}", serde_json::to_string_pretty(&schema).unwrap());
 }
+
+#[cfg(not(feature = "derive"))]
+fn main() {}
diff --git a/examples/remote_derive.rs b/examples/remote_derive.rs
index db01d8d..d25228b 100644
--- a/examples/remote_derive.rs
+++ b/examples/remote_derive.rs
@@ -1,42 +1,47 @@
-// Pretend that this is somebody else's crate, not a module.
-mod other_crate {
-    // Neither Schemars nor the other crate provides a JsonSchema impl
-    // for this struct.
-    pub struct Duration {
-        pub secs: i64,
-        pub nanos: i32,
+#[cfg(feature = "derive")]
+fn main() {
+
+    // Pretend that this is somebody else's crate, not a module.
+    mod other_crate {
+	// Neither Schemars nor the other crate provides a JsonSchema impl
+	// for this struct.
+	pub struct Duration {
+            pub secs: i64,
+            pub nanos: i32,
+	}
     }
-}
 
-////////////////////////////////////////////////////////////////////////////////
+    ////////////////////////////////////////////////////////////////////////////////
 
-use other_crate::Duration;
-use schemars::{schema_for, JsonSchema};
+    use other_crate::Duration;
+    use schemars::{schema_for, JsonSchema};
 
-// This is just a copy of the remote data structure that Schemars can use to
-// create a suitable JsonSchema impl.
-#[derive(JsonSchema)]
-#[serde(remote = "Duration")]
-pub struct DurationDef {
-    pub secs: i64,
-    pub nanos: i32,
-}
+    // This is just a copy of the remote data structure that Schemars can use to
+    // create a suitable JsonSchema impl.
+    #[derive(JsonSchema)]
+    #[serde(remote = "Duration")]
+    pub struct DurationDef {
+	pub secs: i64,
+	pub nanos: i32,
+    }
 
-// Now the remote type can be used almost like it had its own JsonSchema impl
-// all along. The `with` attribute gives the path to the definition for the
-// remote type. Note that the real type of the field is the remote type, not
-// the definition type.
-#[derive(JsonSchema)]
-pub struct Process {
-    pub command_line: String,
-    #[serde(with = "DurationDef")]
-    pub wall_time: Duration,
-    // Generic types must be explicitly specified with turbofix `::<>` syntax.
-    #[serde(with = "Vec::<DurationDef>")]
-    pub durations: Vec<Duration>,
-}
+    // Now the remote type can be used almost like it had its own JsonSchema impl
+    // all along. The `with` attribute gives the path to the definition for the
+    // remote type. Note that the real type of the field is the remote type, not
+    // the definition type.
+    #[derive(JsonSchema)]
+    pub struct Process {
+	pub command_line: String,
+	#[serde(with = "DurationDef")]
+	pub wall_time: Duration,
+	// Generic types must be explicitly specified with turbofix `::<>` syntax.
+	#[serde(with = "Vec::<DurationDef>")]
+	pub durations: Vec<Duration>,
+    }
 
-fn main() {
     let schema = schema_for!(Process);
     println!("{}", serde_json::to_string_pretty(&schema).unwrap());
 }
+
+#[cfg(not(feature = "derive"))]
+fn main() {}
diff --git a/examples/schemars_attrs.rs b/examples/schemars_attrs.rs
index 4ad2503..5212200 100644
--- a/examples/schemars_attrs.rs
+++ b/examples/schemars_attrs.rs
@@ -1,30 +1,34 @@
-use schemars::{schema_for, JsonSchema};
-use serde::{Deserialize, Serialize};
+#[cfg(feature = "derive")]
+fn main() {
+    use schemars::{schema_for, JsonSchema};
+    use serde::{Deserialize, Serialize};
 
-#[derive(Deserialize, Serialize, JsonSchema)]
-#[schemars(rename_all = "camelCase", deny_unknown_fields)]
-pub struct MyStruct {
-    #[serde(rename = "thisIsOverridden")]
-    #[schemars(rename = "myNumber", range(min = 1, max = 10))]
-    pub my_int: i32,
-    pub my_bool: bool,
-    #[schemars(default)]
-    pub my_nullable_enum: Option<MyEnum>,
-    #[schemars(inner(regex(pattern = "^x$")))]
-    pub my_vec_str: Vec<String>,
-}
+    #[derive(Deserialize, Serialize, JsonSchema)]
+    #[schemars(rename_all = "camelCase", deny_unknown_fields)]
+    pub struct MyStruct {
+	#[serde(rename = "thisIsOverridden")]
+	#[schemars(rename = "myNumber", range(min = 1, max = 10))]
+	pub my_int: i32,
+	pub my_bool: bool,
+	#[schemars(default)]
+	pub my_nullable_enum: Option<MyEnum>,
+	#[schemars(inner(regex(pattern = "^x$")))]
+	pub my_vec_str: Vec<String>,
+    }
 
-#[derive(Deserialize, Serialize, JsonSchema)]
-#[schemars(untagged)]
-pub enum MyEnum {
-    StringNewType(#[schemars(phone)] String),
-    StructVariant {
-        #[schemars(length(min = 1, max = 100))]
-        floats: Vec<f32>,
-    },
-}
+    #[derive(Deserialize, Serialize, JsonSchema)]
+    #[schemars(untagged)]
+    pub enum MyEnum {
+	StringNewType(#[schemars(phone)] String),
+	StructVariant {
+            #[schemars(length(min = 1, max = 100))]
+            floats: Vec<f32>,
+	},
+    }
 
-fn main() {
     let schema = schema_for!(MyStruct);
     println!("{}", serde_json::to_string_pretty(&schema).unwrap());
 }
+
+#[cfg(not(feature = "derive"))]
+fn main() {}
diff --git a/examples/serde_attrs.rs b/examples/serde_attrs.rs
index b8d0fb4..9e1a32a 100644
--- a/examples/serde_attrs.rs
+++ b/examples/serde_attrs.rs
@@ -1,24 +1,28 @@
-use schemars::{schema_for, JsonSchema};
-use serde::{Deserialize, Serialize};
+#[cfg(feature = "derive")]
+fn main() {
+    use schemars::{schema_for, JsonSchema};
+    use serde::{Deserialize, Serialize};
 
-#[derive(Deserialize, Serialize, JsonSchema)]
-#[serde(rename_all = "camelCase", deny_unknown_fields)]
-pub struct MyStruct {
-    #[serde(rename = "myNumber")]
-    pub my_int: i32,
-    pub my_bool: bool,
-    #[serde(default)]
-    pub my_nullable_enum: Option<MyEnum>,
-}
+    #[derive(Deserialize, Serialize, JsonSchema)]
+    #[serde(rename_all = "camelCase", deny_unknown_fields)]
+    pub struct MyStruct {
+	#[serde(rename = "myNumber")]
+	pub my_int: i32,
+	pub my_bool: bool,
+	#[serde(default)]
+	pub my_nullable_enum: Option<MyEnum>,
+    }
 
-#[derive(Deserialize, Serialize, JsonSchema)]
-#[serde(untagged)]
-pub enum MyEnum {
-    StringNewType(String),
-    StructVariant { floats: Vec<f32> },
-}
+    #[derive(Deserialize, Serialize, JsonSchema)]
+    #[serde(untagged)]
+    pub enum MyEnum {
+	StringNewType(String),
+	StructVariant { floats: Vec<f32> },
+    }
 
-fn main() {
     let schema = schema_for!(MyStruct);
     println!("{}", serde_json::to_string_pretty(&schema).unwrap());
 }
+
+#[cfg(not(feature = "derive"))]
+fn main() {}
diff --git a/examples/validate.rs b/examples/validate.rs
index 4116976..a60a1d4 100644
--- a/examples/validate.rs
+++ b/examples/validate.rs
@@ -1,24 +1,30 @@
-use schemars::{schema_for, JsonSchema};
+#[cfg(feature = "derive")]
+fn main() {
 
-#[derive(JsonSchema)]
-pub struct MyStruct {
-    #[validate(range(min = 1, max = 10))]
-    pub my_int: i32,
-    pub my_bool: bool,
-    #[validate(required)]
-    pub my_nullable_enum: Option<MyEnum>,
-}
+    use schemars::{schema_for, JsonSchema};
 
-#[derive(JsonSchema)]
-pub enum MyEnum {
-    StringNewType(#[validate(phone)] String),
-    StructVariant {
-        #[validate(length(min = 1, max = 100))]
-        floats: Vec<f32>,
-    },
-}
+    #[derive(JsonSchema)]
+    pub struct MyStruct {
+	#[validate(range(min = 1, max = 10))]
+	pub my_int: i32,
+	pub my_bool: bool,
+	#[validate(required)]
+	pub my_nullable_enum: Option<MyEnum>,
+    }
+
+    #[derive(JsonSchema)]
+    pub enum MyEnum {
+	StringNewType(#[validate(phone)] String),
+	StructVariant {
+            #[validate(length(min = 1, max = 100))]
+            floats: Vec<f32>,
+	},
+    }
 
-fn main() {
     let schema = schema_for!(MyStruct);
     println!("{}", serde_json::to_string_pretty(&schema).unwrap());
 }
+
+#[cfg(not(feature = "derive"))]
+fn main() {}
+
diff --git a/src/gen.rs b/src/gen.rs
index a31839a..7a43f34 100644
--- a/src/gen.rs
+++ b/src/gen.rs
@@ -137,6 +137,8 @@ impl SchemaSettings {
 ///
 /// # Example
 /// ```
+/// #[cfg(feature = "derive")]
+/// {
 /// use schemars::{JsonSchema, gen::SchemaGenerator};
 ///
 /// #[derive(JsonSchema)]
@@ -146,6 +148,7 @@ impl SchemaSettings {
 ///
 /// let gen = SchemaGenerator::default();
 /// let schema = gen.into_root_schema_for::<MyStruct>();
+/// }
 /// ```
 #[derive(Debug, Default)]
 pub struct SchemaGenerator {
@@ -413,6 +416,8 @@ impl SchemaGenerator {
     ///
     /// # Example
     /// ```
+    /// #[cfg(feature = "derive")]
+    /// {
     /// use schemars::{JsonSchema, gen::SchemaGenerator};
     ///
     /// #[derive(JsonSchema)]
@@ -430,6 +435,7 @@ impl SchemaGenerator {
     /// assert!(dereferenced.is_some());
     /// assert!(!dereferenced.unwrap().is_ref());
     /// assert_eq!(dereferenced, gen.definitions().get("MyStruct"));
+    /// }
     /// ```
     pub fn dereference<'a>(&'a self, schema: &Schema) -> Option<&'a Schema> {
         match schema {
diff --git a/src/json_schema_impls/core.rs b/src/json_schema_impls/core.rs
index 955ead6..1f5f846 100644
--- a/src/json_schema_impls/core.rs
+++ b/src/json_schema_impls/core.rs
@@ -196,6 +196,7 @@ mod tests {
     }
 
     #[test]
+    #[cfg(feature = "derive")]
     fn schema_for_option_with_ref() {
         use crate as schemars;
         #[derive(JsonSchema)]
diff --git a/src/lib.rs b/src/lib.rs
index 9e932d5..e9ccb10 100644
--- a/src/lib.rs
+++ b/src/lib.rs
@@ -61,6 +61,8 @@ use schema::Schema;
 /// # Examples
 /// Deriving an implementation:
 /// ```
+/// #[cfg(feature = "derive")]
+/// {
 /// use schemars::{schema_for, JsonSchema};
 ///
 /// #[derive(JsonSchema)]
@@ -69,6 +71,7 @@ use schema::Schema;
 /// }
 ///
 /// let my_schema = schema_for!(MyStruct);
+/// }
 /// ```
 ///
 /// When manually implementing `JsonSchema`, as well as determining an appropriate schema,
diff --git a/src/macros.rs b/src/macros.rs
index 18a6810..9f2718d 100644
--- a/src/macros.rs
+++ b/src/macros.rs
@@ -4,6 +4,8 @@
 ///
 /// # Example
 /// ```
+/// #[cfg(feature = "derive")]
+/// {
 /// use schemars::{schema_for, JsonSchema};
 ///
 /// #[derive(JsonSchema)]
@@ -12,6 +14,7 @@
 /// }
 ///
 /// let my_schema = schema_for!(MyStruct);
+/// }
 /// ```
 #[cfg(doc)]
 #[macro_export]
diff --git a/tests/schema_settings.rs b/tests/schema_settings.rs
index d00042b..a868cbf 100644
--- a/tests/schema_settings.rs
+++ b/tests/schema_settings.rs
@@ -1,3 +1,4 @@
+#![cfg(feature = "derive")]
 mod util;
 use schemars::gen::SchemaSettings;
 use schemars::JsonSchema;
