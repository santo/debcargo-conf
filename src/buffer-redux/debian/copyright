Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: buffer-redux
Upstream-Contact:
 dignifiedquire <me@dignifiedquire.com>
 Austin Bonander <austin.bonander@gmail.com>
Source: https://github.com/dignifiedquire/buffer-redux

Files: *
Copyright:
 2023-2024 dignifiedquire <me@dignifiedquire.com>
 2016-2019 Austin Bonander <austin.bonander@gmail.com>
License: MIT or Apache-2.0

Files: src/lib.rs
Copyright:
 2013 The Rust Project Developers <https://github.com/rust-lang>
 2016-2018 Austin Bonander <austin.bonander@gmail.com>
 2024 dignifiedquire <me@dignifiedquire.com>
License: MIT or Apache-2.0

Files: src/ringbuf_tests.rs
Copyright:
 2013 The Rust Project Developers <https://github.com/rust-lang>
 2018 Austin Bonander <austin.bonander@gmail.com>
 2024 dignifiedquire <me@dignifiedquire.com>
License: MIT or Apache-2.0

Files: src/std_tests.rs
Copyright:
 2013 The Rust Project Developers <https://github.com/rust-lang>
 2016-2018 Austin Bonander <austin.bonander@gmail.com>
 2024 dignifiedquire <me@dignifiedquire.com>
License: MIT or Apache-2.0

Files: debian/*
Copyright:
 2024 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2024 Daniel Kahn Gillmor <dkg@fifthhorseman.net>
License: MIT or Apache-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
