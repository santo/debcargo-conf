Source: rust-rsop
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13),
 dh-sequence-cargo,
 cargo:native,
 rustc:native,
 libstd-rust-dev,
 librust-env-logger-0.11+default-dev,
 librust-rpgpie-sop-0.6+default-dev,
 librust-sop-0.8+cli-dev,
 librust-sop-0.8+default-dev,
 bash-completion
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/rsop]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/rsop
Homepage: https://codeberg.org/heiko/rsop
X-Cargo-Crate: rsop
Rules-Requires-Root: no

Package: rsop
Architecture: any
Multi-Arch: allowed
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 sopv (= 1.0),
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
Static-Built-Using: ${cargo:Static-Built-Using}
Description: Stateless OpenPGP (SOP) CLI tool based on rPGP and rpgpie
 rsop offers a Rust-based implementation of the Stateless OpenPGP
 Command Line Interface.
 .
 This standards-derived interface is an implementation of
 https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/,
 intended to be used in interoperability tests and simple OpenPGP
 commands, while leaving no trace in the filesystem.
 This package contains the following binaries built from the Rust crate
 "rsop":
  - rsop

Package: rsopv
Architecture: any
Multi-Arch: allowed
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 sopv (= 1.0),
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
Static-Built-Using: ${cargo:Static-Built-Using}
Description: Stateless OpenPGP Verification-only (SOPV) CLI (rpgp and rpgpie)
 rsopv offers a Rust-based implementation of the Stateless OpenPGP
 Command Line Interface's verification-only subset (also known as "sopv").
 .
 This standards-derived interface is an implementation of
 https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/,
 intended to be used in interoperability tests and simple OpenPGP
 commands, while leaving no trace in the filesystem.
 This package contains the following binaries built from the Rust crate
 "rsop":
  - rsopv
